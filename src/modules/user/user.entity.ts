import { IsString } from 'class-validator';
import { Column, Entity } from 'typeorm';
import { BaseEntity } from '../../base.entity';

@Entity('users')
export class User extends BaseEntity {
  @IsString()
  @Column()
  code: string;

  @IsString()
  @Column()
  name: string;
}
